﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public enum CameraMode { TOPDOWN, CINEMATIC }

    CameraMode mode = CameraMode.TOPDOWN;
    [SerializeField] Transform target;
    [SerializeField] float targetHeight = 30f; 
    [SerializeField] float positionLerpRate = 30f;
    [SerializeField] float zoomLerpRate = 20f; 
    [SerializeField] float rotationLerpRate = 30f; 

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        
        float dt = Time.deltaTime; 

        switch (mode)
        {
            case CameraMode.TOPDOWN:
                transform.rotation = Quaternion.Euler(90f, 0f, 0f); 
                transform.position = new Vector3(Mathf.Lerp(transform.position.x, target.position.x, dt * positionLerpRate), 
                                                 Mathf.Lerp(transform.position.y, targetHeight, dt * zoomLerpRate), 
                                                 Mathf.Lerp(transform.position.z, target.position.z, dt * positionLerpRate)); 
                break; 
            case CameraMode.CINEMATIC:
                Quaternion lookAtRot = Quaternion.Euler((target.position - transform.position).normalized);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookAtRot, dt * rotationLerpRate);
                //TODO: set position
                break;
        }
	}

    public void SetMode(CameraMode newMode)
    {
        mode = newMode; 
    }
}
