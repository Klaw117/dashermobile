﻿using UnityEngine;
using System.Collections;

public class Dummy_MoveCtrl : MonoBehaviour
{
    [SerializeField] Vector3 minBounds;
    [SerializeField] Vector3 maxBounds;
    NavMeshAgent navAgent; 

	// Use this for initialization
	void Start () {
        navAgent = GetComponent<NavMeshAgent>();
        StartCoroutine(AICheck()); 
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    IEnumerator AICheck()
    {
        while (true)
        {
            float randomTime = Random.Range(1f, 6f);
			
            yield return new WaitForSeconds(randomTime);

            Vector3 randomPos = new Vector3(Random.Range(minBounds.x, maxBounds.x), 
                                            Random.Range(minBounds.y, maxBounds.y), 
                                            Random.Range(minBounds.z, maxBounds.z));
            navAgent.SetDestination(randomPos); 
        }
    }
}
