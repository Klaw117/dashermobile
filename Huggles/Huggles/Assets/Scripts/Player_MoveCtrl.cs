﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 

public class Player_MoveCtrl : MonoBehaviour {

	CharacterController charCtrl;
	NavMeshAgent agent;
    Rigidbody rb;
    Collider collider;

	Vector3 targetPos;
	bool throwSelfAtTarget = false;
    
    bool stunned = false;
    float stunTimer = 0f;
    [SerializeField] float stunTime = 2f; 
	
    [SerializeField] float speed = 1.00f;
    [SerializeField] float stopThreshold = 0.1f; 
	
	// Use this for initialization
	void Start () {
		charCtrl = GetComponent<CharacterController> ();
		targetPos = transform.position;
		//agent = GetComponent<NavMeshAgent> ();
        rb = GetComponent<Rigidbody>();
        collider = GetComponent<SphereCollider>(); 
	}
	
	// Update is called once per frame
	void Update () {

        if (stunTimer > 0f)
        {
            stunned = true;
            stunTimer = Mathf.MoveTowards(stunTimer, 0f, Time.deltaTime);

            GameObject.Find("PlayerStateText").GetComponent<Text>().text = "stunned"; 
        }
        else
        {
            stunned = false;
            GameObject.Find("PlayerStateText").GetComponent<Text>().text = "not stunned";
        }

        

        if (stunned)
        {
            charCtrl.enabled = false;
			collider.isTrigger = false;
            //collider.enabled = true; 
			//agent.enabled = false;
			//agent.Stop();
            rb.isKinematic = false;
            rb.detectCollisions = true; 
        }
        else
        {
            charCtrl.enabled = true;
			collider.isTrigger = true;
            //collider.enabled = false;
			//agent.enabled = true;
			//agent.Resume();
            rb.isKinematic = true;
            rb.detectCollisions = false;
            
            //if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Ended)
            //Vector3 touchPos = new Vector3 (Input.GetTouch(0).position.x, 0, Input.GetTouch(0).position.y);
            if (Input.GetMouseButtonUp(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    targetPos = hit.point;
                    targetPos.y = 0f;
                }

                throwSelfAtTarget = true;
            }

            //if (Input.GetMouseButtonUp (1))
            //	throwSelfAtTarget = false;
            if (Vector3.Distance(transform.position, targetPos) < stopThreshold)
            {
                throwSelfAtTarget = false;
            }

            if (throwSelfAtTarget)
            {
                Vector3 diff = targetPos - transform.position;

                //Vector3 rot = new Vector3 (0, Vector3.Angle (transform.position, targetPos), 0);

                transform.LookAt(new Vector3(targetPos.x, transform.position.y, targetPos.z), Vector3.up);
                Vector3 move = Vector3.Lerp(Vector3.zero, diff, Time.deltaTime * speed);
                charCtrl.Move(move);
            }
        }
	}

	void OnControllerColliderHit (ControllerColliderHit hit) {
		GameObject other = hit.gameObject;

		if (other.tag == "Wall") {
			throwSelfAtTarget = false;
			Debug.Log("hit a thing");
		} 
        else if (other.tag == "Repulsor") {
            throwSelfAtTarget = false;
            Debug.Log("hit bumper car");
            
            stunTimer = stunTime;
            StartCoroutine(Bump(other.transform.position)); 
        }
        else if (other.tag == "DumDum") {
            throwSelfAtTarget = false;
            Debug.Log("hit dummy"); 
            Vector3 pos = other.transform.position;
            Quaternion rot = other.transform.rotation; 
            Destroy(other);
			GameObject.FindWithTag(Tags.game_manager).GetComponent<GameManager>().incrementScore(100);
            GameObject obj = (GameObject)Instantiate(Resources.Load("Prefabs/DummyDead"), pos, rot);
            //obj.GetComponent<Rigidbody>().AddExplosionForce(120f, transform.TransformPoint(Random.Range(-0.5f, 0.5f), 0f, Random.Range(-0.5f, 0.5f)), 10f); 
            obj.GetComponent<Rigidbody>().AddExplosionForce(1024f, pos + new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)), 10f); 
        }
	}

	void OnTriggerEnter (Collider hit) {
		//GameObject other = coll.gameObject;
		GameObject other = hit.gameObject;

		if (other.tag == "Repulsor") {
			throwSelfAtTarget = false;
			Debug.Log("hit bumper car");
			
			stunTimer = stunTime;
			StartCoroutine(Bump(other.transform.position)); 
		}
	}

    IEnumerator Bump(Vector3 pos)
    {
        yield return new WaitForSeconds(0.1f); 
        rb.AddExplosionForce(1024f, pos, 30f); 
    }
}
