﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIController : MonoBehaviour {
	[SerializeField] Text scoreText;
	GameManager gameManager;

	// Use this for initialization
	void Start () {
		gameManager = null;
		StartCoroutine(findGameManager());
	}

	IEnumerator findGameManager() {
		while (gameManager == null) {
			yield return new WaitForSeconds (0.1f);
			if (GameObject.FindWithTag (Tags.game_manager) != null) {
				gameManager = GameObject.FindWithTag (Tags.game_manager).GetComponent<GameManager> ();
			}
			else {
				gameManager = null;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (gameManager != null) {
			scoreText.text = gameManager.getScore().ToString();
		}
	}
}
