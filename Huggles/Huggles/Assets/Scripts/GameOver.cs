﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		GUI.Label(new Rect(Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/4), "<size=96>Game Over</size>");
		if (GUI.Button (new Rect (2 * Screen.width / 5, 3 * Screen.height / 5, Screen.width / 5, Screen.height / 7), "<size=36>Restart</size>"))
			Application.LoadLevel (0);
	}
}
