﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	uint score = 0;

	// Use this for initialization
	void Start () {
		if (this.tag == Tags.game_manager_new) {
			if (GameObject.FindWithTag(Tags.game_manager) == null) {
				this.tag = Tags.game_manager;
				DontDestroyOnLoad(this.gameObject);
			}
			else {
				DestroyObject(this.gameObject);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public uint getScore() {
		return score;
	}

	public void setScore(uint score) {
		this.score = score;
	}

	public void incrementScore(uint score) {
		this.score += score;
	}
}
