﻿using UnityEngine;
using System.Collections;

public class TimerController : MonoBehaviour {

    float timeLeft = 10.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (timeLeft > 0)
			timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
			Application.LoadLevel(1);
	}

    void OnGUI() {
        GUI.Box(new Rect(10, 10, 50, 20), "" + timeLeft.ToString("0"));
    }
}
